<!-- PROJECT LOGO -->
<br />
<p align="justify">
  <a href="https://github.com/github_username/repo">
    <img src="assets/images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="justify">JavaScript Starter Web-App</h3>

  <p align="justify">
    IN PROGRESS<br/>
    A JavaScript starter project that automates the key things you need to build and deploy a standards compliant web-app (Yaks have been shaved).
    <br />
    <a href="https://gitlab.com/EssentialistDev/blank-js-web-app/-/wikis/home"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://js-starter-webapp.herokuapp.com/">View Demo</a>
    ·
    <a href="https://gitlab.com/EssentialistDev/blank-js-web-app/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/EssentialistDev/blank-js-web-app/-/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements) -->


<!-- ABOUT THE PROJECT -->
## About The Project

![JavaScript Starter App Screen Shot](#)

Setting up a web-app dev environment from scratch is a bit of a nightmare. There are lots of moving pieces, and documentation tends to be a bit overwhelming or filled with lots of unecessary steps.

The purpose of this starter project is to make it easier for you (beginner-ish level up) to write a web-app that lets you:

- Prioritize accessibility with the help of automated accessibility testing.
- Test-drive your code (first test example included for both front and back-end).
- Write consistent CSS and semantic HTML (clean, short and extendable styleguide included).
- Write ES6 standards-compliant JavaScript with the help of a auto linter which tells you what could be changed.
- Seperate concerns (style, structure, behavior) with guideline examples.
- Deploy your code both locally and to production (Heroku).

If you've never written a web-app before, then you can follow my step-by-step guide for building a [bookmarking web-app for beginners](#), using this starter project as a base.

### Built With

- [Sass](https://sass-lang.com/) for splitting styles for better organizing and decoupling style from structure.
- [JavaScript (MDN LINK)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference) for writing browser code (client-side).
- [Node](https://nodejs.org/en/) for writing JavaScript on the server side.
- [Mocha](https://mochajs.org/) for running back-end tests.
- [Chai](https://www.chaijs.com/) for writing test assertions (that read better than default mocha assertions).
- [http-server](https://www.npmjs.com/package/http-server) for a quick command-line web-server for localhost development.
- [pa11y-ci](https://github.com/pa11y/pa11y-ci) for automating web accessibility testing.

## Getting Started

To get a local copy up and running, follow these steps.

### Prerequisites

You need Node and NPM to be able to install and use this starter project. I use [Homebrew](https://brew.sh/) to install these.

- node and npm (npm will be installed with node).

`brew install node`

## Installation

1. Clone the repo

`git clone https://gitlab.com/EssentialistDev/javascript-starter-web-app.git`

2. Install NPM packages

`npm install`

## Usage

### Recommendations:

#### While working on your project (making file changes):

- Run your **web-server** so you can see what you're working on in your browser).
- Run your **tests** (mocha and chai) to make sure everything is passing (auto updates on every file change).
- Run your **compile-sass** command (which auto compiles your sass into css on every file change).

#### Before committing changes:

- Run your **accessibility** tests to make sure your markup and sass meet accessibility standards.
- Run your **lint** tests to make sure your JavaScript is ES6 compliant.

#### Commit messages

Write your commit messages using the [imperative mood](https://chris.beams.io/posts/git-commit/), like this:

if applied, this commit will: [do something].

Some examples:

- 🙈 Ignore auto-generated files. (ignoring node_modules)
- 💄 Tell browser where to find styles. (adding main stylesheet)
- ➕ Ensure ES6 standards are met. (adding ESLint dependency).

I like using [Gitmoji](https://gitmoji.carloscuesta.me/) for adding emojis to the start of my commit message. Each emoji represents a task type. I use the following most frequently:

- ✅ Test-related
- ♻️  Refactor
- 🙈 Ignoring files
- 🛠 Config updates
- ➕ Dependencies
- 🚧 Work in progress
- 💄 UI changes
- 🐛 Fix bugs
- ✏️  Fix typos
- 📝 Write docs
- 🗄 Database-related
- 🔥 Remove code/files
- 🚚 Move/rename files
- 🚀 Deploy

### Commands (package.json scripts)

These are all the commands for running your tests, accessibility and standards checks, as well as deploying to your local web-server. For deploying to a production server, see the next section.

#### Start your local web-server

`npm run start`

This command opens up your project in your localhost server. The output from running this command will tell you which localhost url to go to.

#### Auto run accessibily Tests

`npm run test-accessibility`

You can check the accessibility of your markup and styles at any time by running the above command.

At the moment, it's configured to WCAG level AAA, which is the current highest standards level.


#### Auto run your JavaScript tests

`npm run tests`

#### Auto compile your Sass

`npm run compile-sass`

#### Check JavaScript for ECMAScript 6 errors

`npm run test-lint`

## Deploy to Production

[Heroku](https://dashboard.heroku.com/) is my go-to place for deploying web-apps. You can deploy as many web-apps as you want on their server for free, with limitations like going to sleep when not in use for 30 mins. Perfect for practicing deploying reguarly.

### Prerequisites

- Node and npm (covered already)
- An existing Node.js app (this starter app counts)
- A free [Heroku account](https://signup.heroku.com/signup/dc)
- The [Heroku CLI](https://cli.heroku.com/)

`brew tap heroku/brew && brew install heroku`

### Create a Heroku app

In the root of this starter app project (same place package.json can be found):

- The following command creates an place for your web-app to live on heroku. Give your web-app a name that represents what it'll be used for. If you don't know yet, just call it js-starter-web-app. You can rename it later if you want to (`heroku apps:rename newname`). 

`heroku create name-of-your-app`

- Deploy to Heroku

`git push heroku master`

- Open your site

`heroku open`

If you have any problems deploying your web-app to Heroku, [DM me on Twitter @becca9941](https://twitter.com/Becca9941). It's the fastest way to get a response (my time zone is AEST Sydney).

### Resources

This starter project takes care of the yak-shaving which makes it difficult to set up a decent web-app developer environment. But there's a lot more that goes into building a web-app.

I'm writing a series of tutorials for doing that, but in the mean time, here are some resources that I personally find to be excellent:

- [README Template](https://github.com/othneildrew/Best-README-Template) for helping you document how to install and use your project, and lots more.
- [Priority Guides: A content first alternative to wireframes](https://alistapart.com/article/priority-guides-a-content-first-alternative-to-wireframes/) for avoiding wireframe pitfalls (early visual designs tend to stick and limit creativity, conversations become more about presentation than the content itself etc).
- [Block Element Modifier (BEM)](http://getbem.com/naming/) naming convention for writing semantic class names. I wrote a tweet thread demonstrating [how to write semantic BEM class names](https://twitter.com/Becca9941/status/1295940486591418368).
- [Atlassian Design System Template](https://www.atlassian.com/software/confluence/templates/design-system) for helping you stay consistent in your design and implementation choices. This is a great template.
- [TDD (backend)](https://online-training.jbrains.ca/p/wbitdd-01) for teaching you how to get started with test-driving your back-end code. Scroll down the page a fair bit until you get to the list of lessons, the first few are free.


## Your Requests / Issues

See the [open issues](https://gitlab.com/EssentialistDev/blank-js-web-app/-/issues) for a list of proposed tutorials (and known issues).

If you'd like a specific tutorial for something, like creating a custom styleguide, test approaches, design process, workflows etc, this is the best place to recommend them.

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Becca - [@becca9941](https://twitter.com/Becca9941)

Project Link: [https://gitlab.com/EssentialistDev/javascript-starter-web-app.git](https://gitlab.com/EssentialistDev/javascript-starter-web-app.git)

## Acknowledgements

- [Andy Palmer](https://twitter.com/AndyPalmer) for being my go-to for growth-inspiring feedback.
- [J.B. Rainsberger](https://twitter.com/jbrains) for inspiring me to create a developer environment that makes it easier to do things right.
